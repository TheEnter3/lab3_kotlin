object Calculations {
    fun positionGeometricCenter(points: Array<Point2D?>): Point2D {
        var newX = 0.0
        var newY = 0.0
        for (point in points) {
            newX += point!!.x
            newY += point.y
        }

        return Point2D(newX / points.size, newY / points.size)
    }

    fun positionCenterOfMass(materialPoints: Array<MaterialPoint2D?>): Point2D {
        var newX = 0.0
        var newY = 0.0
        var newMass = 0.0
        for (materialPoint in materialPoints) {
            newX += materialPoint!!.mass * materialPoint.x
            newY += materialPoint.mass * materialPoint.y
            newMass += materialPoint.mass
        }
        return MaterialPoint2D(newX / newMass, newY / newMass, newMass)
    }
}
