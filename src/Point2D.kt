open class Point2D( val x: Double, val y: Double) {
    override fun toString(): String {
        return "x = $x, y = $y"
    }
}
