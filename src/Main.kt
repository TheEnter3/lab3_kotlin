
        fun main(args: Array<String>) {
            val POINTS = 2
            val points: Array<Point2D?> = arrayOfNulls<Point2D>(POINTS)
            val materialPoints: Array<MaterialPoint2D?> = arrayOfNulls<MaterialPoint2D>(POINTS)
            points[0] = Point2D(0.0, 0.0)
            points[1] = Point2D(10.0, 10.0)
            materialPoints[0] = MaterialPoint2D(0.0, 0.0, 10.0)
            materialPoints[1] = MaterialPoint2D(10.0, 10.0, 100.0)
            val geometricCenter: Point2D = Calculations.positionGeometricCenter(points)
            val massCenter: Point2D = Calculations.positionCenterOfMass(materialPoints)
            println("Polozenie srodka masy: $massCenter")
            println("Polozenie srodka geometrycznego: $geometricCenter")
        }

